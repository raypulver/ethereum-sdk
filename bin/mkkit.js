#!/usr/bin/env node
'use strict';

const fs = require('fs-extra');
const path = require('path');
const _ = require('lodash');
const { EOL } = require('os');

const thisDir = process.cwd();
const ethKitDir = path.join(__dirname, '..');

const omit = {
  './': [
    'utils',
    'web3-utils',
    'wallet',
    'internal',
    'wrap.js',
    'wrap-rpc.js',
    'webpack.config.js',
    'test',
    'shh',
    'rpc-method.js',
    'rpc-call.js',
    'package.json',
    'node_modules',
    'package-lock.json',
    'internal',
    'index.js',
    'init',
    'id-map',
    'dist',
    'bin',
    'abi'
  ]
};

(async () => {
  const files = await Promise.all([
    'internal',
    'utils',
    'web3-utils',
    'wallet',
    './'
  ].map(async (base) => {
    await fs.mkdirp(path.join(thisDir, base));
    const filesToSearch = (await fs.readdir(path.join(ethKitDir, base))).filter((v) => !(omit[base] || []).includes(v));
     
    return Promise.all(filesToSearch.map((file) => ({
      filename: path.join(ethKitDir, base, file),
      target: path.join(thisDir, base, file),
      contents: "'use strict';" + EOL + EOL +
        "module.exports = require('ethereum-kit/" + path.join(base, path.parse(file).name) + "');"
     })).map(async ({
       target,
       contents
     }) => {
       if (!await fs.exists(target)) await fs.writeFile(target, contents);
     }));
  }));
})().catch((err) => console.error(err.stack));
