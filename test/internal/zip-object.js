'use strict';

const {expect} = require('chai');
const zipObject = require('../../internal/zip-object');

module.exports = [
  'should zipObject',
  () => {
    const zipA = zipObject(['a']);
    const out = zipA([1]);
    expect(out).to.eql({a: 1});
  },
];
