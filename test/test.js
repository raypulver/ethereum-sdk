'use strict';

const fs = require('fs');
const path = require('path');
require('chai').use(require('chai-spies'));

(() => process.env.KOVAN ? require('./kovan')(describe, it) : fs.readdirSync(__dirname).forEach((v) => {
  if (v === 'kovan') return;
  if (fs.lstatSync(path.join(__dirname, v)).isDirectory()) {
    const [ description, suite ] = require(path.join(__dirname, v));
    describe(description, suite(it));
  }
}))();

