'use strict';

const requireAll = require('../../internal/require-all');
const forOwn = require('../../internal/for-own');

module.exports = [
  'ethereum-kit web3-utils shim',
  it => () =>
    forOwn(requireAll(__dirname, 'index.js'), ([description, suite]) =>
      it(description, suite),
    ),
];
