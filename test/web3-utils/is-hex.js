'use strict';

const { expect } = require('chai');
const isHex = require('../../web3-utils/is-hex');

module.exports = ['should isHex', () => {
  expect(isHex('0x00')).to.be.true;
  expect(isHex('0xg')).to.be.false;
}];
