'use strict';

const asciiToHex = require('../../web3-utils/ascii-to-hex');
const {
  asciiToHex: asciiToHexWeb3
} = require('web3-utils');
const { expect } = require('chai');
module.exports = ['should do asciiToHex', () => {
  expect(asciiToHexWeb3('woopdoop')).to.eql(asciiToHex('woopdoop'));
}];
