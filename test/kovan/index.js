'use strict';

const { expect } = require('chai');
const fromPrivateKey = require('../../wallet/from-private-key');
const fs = require('fs-extra');
const makeTransaction = require('../../make-transaction');
const getUnitHex = require('../../utils/get-unit-hex');
const setupBlockSubscription = require('../../setup-block-subscription');
const rpc = { 
  provider: 'https://kovan.infura.io/aR7WPNCrZhhnYRnn8yRT'
};
const getEtherBalanceHex = require('../../get-ether-balance-hex');
const getTransactionCount = require('../../get-transaction-count');
const Rational = require('../../rational');
const getUnit = require('../../utils/get-unit');

module.exports = (describe, it) => {
  describe('kovan', () => {
    it('should get a transaction hash', async () => {
      const loadedWallet = fromPrivateKey(Buffer.from('5744526d1682c86ce4da6e4d5102d1a3a56d1a143dfa4a8e8ac37007adcd43bc', 'hex'));
      const secondWallet = fromPrivateKey(Buffer.from('5744526d1664a6bfe4da6e4d5102d1a3a56d1a143dfa4a8e8ac37007adcd441a', 'hex'));
      const tx = makeTransaction(rpc, {
        gasPrice: getUnitHex('1 gwei'),
        gasLimit: getUnitHex('53000'),
        nonce: await getTransactionCount(rpc, loadedWallet.getAddressString()),
        value: getUnitHex('1 finney'),
        chainId: 42,
        to: secondWallet.getAddressString()
      });
      const txHash = tx.getTransactionHash();
      const actualTxHash = await tx.sign(loadedWallet.getPrivateKey()).send();
      const poller = setupBlockSubscription(rpc);
      poller.startPollingForBlocks();
      await poller.getMinedPromise(actualTxHash);
      poller.stopPollingForBlocks();
      await makeTransaction(rpc, {
        gasPrice: getUnitHex('1 gwei'),
        gasLimit: getUnitHex('21000'),
        value: Rational(await getEtherBalanceHex(rpc, secondWallet.getAddressString())).minus(Rational(21000).multiply(getUnit('1 gwei'))).toHexString(),
        to: loadedWallet.getAddressString(),
        nonce: await getTransactionCount(rpc, secondWallet.getAddressString())
      }).sign(secondWallet.getPrivateKey()).send();
    }).timeout(100000);
  });
};
