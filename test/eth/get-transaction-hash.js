'use strict';

const Rational = require('../../rational');
const generate = require('../../wallet/generate');

const makeTransaction = require('../../make-transaction');
const getAccounts = require('../../get-accounts');
const getTransactionCount = require('../../get-transaction-count');
const { expect } = require('chai');
const getUnitHex = require('../../utils/get-unit-hex');
const getUnit = require('../../utils/get-unit');

module.exports = ['should calculate the transaction hash', async () => {
  const rpc = { provider: 'http://localhost:8545' };
  const receiverWallet = generate();
  const [ builtInWallet ] = await getAccounts(rpc);
  await makeTransaction(rpc, {
    gasPrice: getUnitHex('1 gwei'),
    gasLimit: getUnitHex('21000'),
    value: getUnitHex('10 finney'),
    from: builtInWallet,
    to: receiverWallet.getAddressString()
  }).sendTransaction();
  const tx = makeTransaction(rpc, {
    gasPrice: getUnitHex('1 gwei'),
    gasLimit: getUnitHex('21000'),
    value: Rational(getUnit('5 finney')).minus(Rational(getUnit('1 gwei')).multipliedBy('21000')).toHexString(),
    to: builtInWallet,
    nonce: await getTransactionCount(rpc, receiverWallet.getAddressString())
  }).sign(receiverWallet.getPrivateKey());
  const txHashComputed = tx.getTransactionHash();
  await tx.send();
  const secondTx = makeTransaction(rpc, {
    gasPrice: getUnitHex('1 gwei'),
    gasLimit: getUnitHex('21000'),
    value: Rational(getUnit('5 finney')).minus(Rational(getUnit('1 gwei')).multipliedBy('21000')).toHexString(),
    to: builtInWallet,
    nonce: await getTransactionCount(rpc, receiverWallet.getAddressString())
  }).sign(receiverWallet.getPrivateKey());
//  }).sign(receiverWallet.getPrivateKey());
  const secondTxHashComputed = secondTx.getTransactionHash();
  expect(secondTxHashComputed).to.eql(await secondTx.send());
}]; 
