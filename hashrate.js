'use strict';

const rpcMethod = require('./rpc-method')
const hashrate = rpcMethod('eth_hashrate');

module.exports = hashrate;
