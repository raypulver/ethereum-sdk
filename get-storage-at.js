'use strict';

const rpcMethod = require('./rpc-method');

const gasPrice = rpcMethod('eth_getStorageAt');

module.exports = gasPrice;
