'use strict';

const call = require('./call');
const wrap = require('./wrap');

module.exports = wrap.wrapAddress(call);
