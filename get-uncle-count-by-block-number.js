'use strict';

const rpcMethod = require('./rpc-method');
const getUncleCountByBlockNumber = rpcMethod('eth_getUncleCountByBlockHash');

module.exports = getUncleCountByBlockNumber;
