'use strict';

const rpcMethod = require('./rpc-method');
const getUncleCountByBlockHash = rpcMethod('eth_getUncleCountByBlockHash');

module.exports = getUncleCountByBlockHash;
