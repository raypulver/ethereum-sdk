'use strict';

const getFilterChanges = require('../rpc-method')('shh_getFilterChanges');

module.exports = getFilterChanges;
