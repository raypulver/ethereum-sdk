'use strict';

const getBlockTransactionCountByHash = require('./get-block-transaction-count-by-hash');
const getBlockTransactionCountByNumber = require('./get-block-transaction-count-by-number');

const getBlockTransactionCount = (rpc, input) => typeof input === 'number' ? getBlockTransactionCountByNumber(rpc, input) : getBlockTransactionCountByHash(rpc, input);

module.exports = getBlockTransactionCount;
