'use strict';

const mapToShifted = require('./internal/map-to-shifted');
const restoreConstructor = require('./internal/restore-constructor');

function EthereumRPC(provider) {
  if (!(this instanceof EthereumRPC)) return new EthereumRPC(provider);
  this.init(provider);
}

Object.assign(EthereumRPC, {
  idMap: require('./id-map'),
  unitMap: require('./unit-map')
});

const EthereumRPCSuperProto = mapToShifted({
  getProvider: require('./get-provider'),
  setProvider: require('./set-provider'),
  setInterceptor: require('./set-interceptor'),
  getInterceptor: require('./get-interceptor'),
  init: require('./init')
});

EthereumRPC.prototype = Object.create(EthereumRPCSuperProto);
restoreConstructor(EthereumRPC);

Object.assign(EthereumRPC.prototype, mapToShifted({
  rpcCall: require('./rpc-call'),
  setupBlockSubscription: require('./setup-block-subscription'),
  callForBlockToAddress: require('./call-for-block-to-address'),
  callForBlockToNumber: require('./call-for-block-to-number'),
  callForBlockToRational: require('./call-for-block-to-rational'),
  callForBlock: require('./call-for-block'),
  callToAddress: require('./call-to-address'),
  callToNumber: require('./call-to-number'),
  callToPrecision: require('./call-to-precision'),
  callToRational: require('./call-to-rational'),
  call: require('./call'),
  testForAllowance: require('./test-for-allowance'),
  getAccounts: require('./get-accounts'),
  getAllowance: require('./get-allowance'),
  getBlockNumberHex: require('./get-block-number-hex'),
  getBlockNumber: require('./get-block-number'),
  getCode: require('./get-code'),
  getEtherBalanceForBlockHex: require('./get-ether-balance-for-block-hex'),
  getEtherBalanceForBlock: require('./get-ether-balance-for-block'),
  getEtherBalanceHex: require('./get-ether-balance-hex'),
  getEtherBalance: require('./get-ether-balance'),
  getTokenBalance: require('./get-token-balance'),
  getTransactionCountForBlockHex: require('./get-transaction-count-for-block-hex'),
  getTranasctionCountForBlock: require('./get-transaction-count-for-block'),
  getTransactionCountHex: require('./get-transaction-count-hex'),
  getTransactionCount: require('./get-transaction-count'),
  getTransactionReceipt: require('./get-transaction-receipt'),
  sendRawTransaction: require('./send-raw-transaction'),
  sendSignedTransaction: require('./send-signed-transaction'),
  sendTransaction: require('./send-transaction'),
  sendTxFromObject: require('./send-tx-from-object'),
  ownerOfNFT: require('./owner-of-nf-token'),
  getBlock: require('./get-block'),
  isUsingBuiltIn: require('./is-using-built-in'),
  setIsUsingBuiltIn: require('./set-is-using-built-in'),
  makeTransaction: require('./make-transaction'),
  fetchNonceMakeTransaction: require('./fetch-nonce-make-transaction'),
  estimateGas: require('./estimate-gas'),
  getClientVersion: require('./client-version'),
  coinbase: require('./coinbase'),
  getGasPrice: require('./gas-price'),
  getFilterChanges: require('./get-filter-changes'),
  getFilterLogs: require('./get-filter-logs'),
  getHex: require('./get-hex'),
  getLogs: require('./get-logs'),
  getStorageAt: require('./get-storage-at'),
  getString: require('./get-string'),
  getTransactionByBlockHashAndIndex: require('./get-transaction-by-block-hash-and-index'),
  getTransactionByBlockNumberAndIndex: require('./get-transaction-by-block-number-and-index'),
  getTransactionByHash: require('./get-transaction-by-hash'),
  getUncleByBlockHashAndIndex: require('./get-uncle-by-block-hash-and-index'),
  getUncleByBlockNumberAndIndex: require('./get-uncle-by-block-number-and-index'),
  getUncleCountByBlockHash: require('./get-uncle-count-by-block-hash'),
  getUncleCountByBlockNumber: require('./get-uncle-count-by-block-number'),
  getUncleCount: require('./get-uncle-count'),
  getWork: require('./get-work'),
  getHashRate: require('./hashrate'),
  isMining: require('./mining'),
  netListening: require('./net-listening'),
  netPeerCount: require('./net-peer-count'),
  netVersion: require('./net-version'),
  newBlockFilter: require('./new-block-filter'),
  newFilter: require('./new-filter'),
  newPendingTransactionFilter: require('./new-pending-transaction-filter'),
  protocolVersion: require('./protocol-version'),
  putHex: require('./put-hex'),
  putString: require('./put-string'),
  sign: require('./sign'),
  submitHashRate: require('./submit-hashrate'),
  submitWork: require('./submit-work'),
  getSyncing: require('./syncing'),
  uninstallFilter: require('./uninstall-filter')
}));

Object.assign(module.exports, {
  EthereumRPC
});
