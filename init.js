'use strict';

const flow = require('./internal/flow');
module.exports = flow(
  (rpc, provider) => rpc.setProvider(provider)
)
