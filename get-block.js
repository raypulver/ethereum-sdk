'use strict';

const rpcCall = require('./rpc-call');
const Rational = require('tough-rational');

module.exports = (rpc, blockNumber, flag = false) =>
  rpcCall(rpc, 'eth_getBlockByNumber', [
    typeof blockNumber === 'number' ? Rational(blockNumber).toHexString() : blockNumber,
    flag
  ]);
