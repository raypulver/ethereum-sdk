'use strict';

const {
  fromExtendedPrivateKey
} = require('ethereumjs-wallet');
const toBuffer = require('../utils/to-buffer');

const flow = require('../internal/flow');

module.exports = flow(toBuffer, fromExtendedPrivateKey);
