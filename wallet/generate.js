'use strict';

const {
  generate
} = require('ethereumjs-wallet');

module.exports = generate;
