'use strict';

const {
  generateVanityAddress
} = require('ethereumjs-wallet');

module.exports = generateVanityAddress;
