'use strict';

const get = require('../internal/get');
const flow = require('../internal/flow');
const equals = require('../internal/equals');
const isBigNumber = flow(
  get(['constructor', 'name']),
  equals('BigNumber')
);

module.exports = isBigNumber;
