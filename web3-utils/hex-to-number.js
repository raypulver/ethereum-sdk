'use strict';

const {
  hexToNumber
} = require('web3-utils');

module.exports = hexToNumber;
