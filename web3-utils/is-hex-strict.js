'use strict';

const isNumber = require('../internal/is-number');
const isString = require('../internal/is-string');

module.exports = (hex) => (isString(hex) || isNumber(hex)) && /^(-)?0x[0-9a-f]*$/i.test(hex);
