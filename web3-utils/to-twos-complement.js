'use strict';

const {
  toTwosComplement
} = require('web3-utils');

module.exports = toTwosComplement;
