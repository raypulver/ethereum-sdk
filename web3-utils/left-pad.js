'use strict';

const { join } = [];
const { max } = Math;

module.exports = (s, n, c = '0') => join.call(Array(max(n - s.length + 1, 1)), c) + s;
