'use strict';

const padLeft = require('./pad-left');
const {
  join,
  map
} = Array.prototype;

const asciiToHex = (s) => !s ? '0x00' : '0x' + join.call(map.call(s, (v) => padLeft((Number(v.charCodeAt(0)).toString(16)), 2, '0')), '');

module.exports = asciiToHex;
