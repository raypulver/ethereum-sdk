'use strict';

const {
  toWei
} = require('web3-utils');

module.exports = toWei;
