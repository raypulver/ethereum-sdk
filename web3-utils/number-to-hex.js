'use strict';

const {
  numberToHex
} = require('web3-utils');

module.exports = numberToHex;
