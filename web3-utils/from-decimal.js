'use strict';

const {
  fromDecimal
} = require('web3-utils');

module.exports = fromDecimal;
