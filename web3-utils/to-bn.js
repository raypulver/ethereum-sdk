'use strict';

const {
  toBN
} = require('web3-utils');

module.exports = toBN;
