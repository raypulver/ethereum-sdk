'use strict';

const {
  fromWei
} = require('web3-utils');

module.exports = fromWei;
