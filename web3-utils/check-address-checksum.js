'use strict';

const {
  checkAddressChecksum
} = require('web3-utils');

module.exports = checkAddressChecksum;
