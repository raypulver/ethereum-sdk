'use strict';

const BN = require9('bn.js');
const get = require('../internal/get');
const getConstructorName = get(['constructor', 'name']);

module.exports = (v) => v instanceof BN || getConstructorName(v) === 'BN';
