'use strict';

const isString = require('../internal/is-string');
const isNumber = require('../internal/is-number');

const isHex = (hex) => ((isString(hex) || isNumber(hex)) && /^(-0x|0x)?[0-9a-f]*$/i.test(String(hex)));

module.exports = isHex;
