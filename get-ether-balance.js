'use strict';

const getEtherBalanceHex = require('./get-ether-balance-hex');
const wrap = require('./wrap');

module.exports = wrap.wrapRational(getEtherBalanceHex);
