'use strict';

const getUncleCountByBlockNumber = require('./get-uncle-count-by-block-number');
const getUncleCountByBlockHash = require('./get-uncle-count-by-block-hash');

module.exports = (rpc, input) => typeof input === 'number' ? getUncleCountByBlockNumber(rpc, input) : getUncleCountByBlockHash(rpc, input);
