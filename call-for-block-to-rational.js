'use strict';

const wrap = require('./wrap');
const callForBlock = require('./call-for-block');

module.exports = wrap.wrapRational(callForBlock);
