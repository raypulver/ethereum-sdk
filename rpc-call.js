'use strict';

const axios = require('axios');
const RPCError = require('./error');
const idMap = require('./id-map');
const getInterceptor = require('./get-interceptor');
const getProvider = require('./get-provider');
const wrapRpc = require('./wrap-rpc');
const identity = require('./internal/identity');

module.exports = async (rpc, method, params = []) => {
  const wrappedRpc = wrapRpc(rpc);
  const interceptor = getInterceptor(wrappedRpc);
  const response = await axios((typeof interceptor === 'function' ? interceptor : identity)({
    method: 'POST',
    url: getProvider(wrappedRpc),
    data: {
      jsonrpc: '2.0',
      id: idMap.getNextThenIncrement(getProvider(wrappedRpc)),
      method,
      params
    }
  }));
  const { error } = response;
  if (error) throw RPCError(error.message, error.code);
  if (response.data.error) throw RPCError(response.data.error.message, response.data.error.code);
  return response.data.result;
};
