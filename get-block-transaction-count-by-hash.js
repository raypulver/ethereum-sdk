'use strict';

const rpcMethod = require('./rpc-method');
const getBlockTransactionCountByHash = rpcMethod('eth_getBlockTransactionCountByHash');

module.exports = getBlockTransactionCountByHash;
