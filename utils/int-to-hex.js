'use strict';

const intToHex = (i) => '0x' + i.toString(16);
module.exports = intToHex;
