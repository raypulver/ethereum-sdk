'use strict';

const getUnitAsRational = require('./get-unit-as-rational');
const flow = require('../internal/flow');
const method = require('../internal/method');

module.exports = flow(getUnitAsRational, method('toHexString'));
