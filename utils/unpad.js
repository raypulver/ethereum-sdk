'use strict';

const stripHexPrefix = require('./strip-hex-prefix');

const unpad = (a) => {
  a = stripHexPrefix(a);
  var first = a[0];
  while (a.length > 0 && first.toString() === '0') {
    a = a.slice(1);
    first = a[0];
  }
  return a;
};

module.exports = unpad;
