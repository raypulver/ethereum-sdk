'use strict';

const toBuffer = require('./to-buffer');
const bufferToHex = require('./buffer-to-hex');
const Rational = require('tough-rational');

const isValidSignature = (v, r, s, homestead) => {
  r = toBuffer(r);
  s = toBuffer(s);
  const SECP256K1_N_DIV_2 = Rational('0x7fffffffffffffffffffffffffffffff5d576e7357a4501ddfe92f46681b20a0');
  const SECP256K1_N = Rational('0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141');
  if (r.length !== 32 || s.length !== 32) return false;
  if (v !== 27 && v !== 28) return false;
  r = Rational(bufferToHex(r));
  s = Rational(bufferToHex(s));
  if (r.isZero() || r.gt(SECP256K1_N) || s.isZero() || s.gt(SECP256K1_N)) return false;
  if (homestead === false && s.gt(SECP256K1_N_DIV_2)) return false;
  return true;
};

module.exports = isValidSignature;
