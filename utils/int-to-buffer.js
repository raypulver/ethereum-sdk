'use strict';

const intToHex = require('./int-to-hex');
const {
  Buffer: {
    from: bufferFrom
  }
} = require('safe-buffer');
const padToEven = require('./pad-to-even');
const { slice } = String.prototype;
const intToBuffer = (i) => bufferFrom(padToEven(slice.call(intToHex(i), 2)), 'hex');
module.exports = intToBuffer;
