'use strict';

const zeros = require('./zeros');
const toBuffer = require('./to-buffer');

const setLength = (msg, length, right) => {
  var buf = zeros(length);
  msg = toBuffer(msg);
  return right ? msg.length < length ? (msg.copy(buf), buf) : msg.slice(0, length) :msg.length < length ? (msg.copy(buf, length - msg.length), buf) : msg.slice(-length);
};
