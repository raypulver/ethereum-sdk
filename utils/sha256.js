'use strict';

const bufferToHex = require('./buffer-to-hex');
const createHash = require('create-hash');
const toBuffer = require('./to-buffer');
const sha256 = (a) => bufferToHex(createHash('sha256').update(toBuffer(a)).digest());

module.exports = sha256;
