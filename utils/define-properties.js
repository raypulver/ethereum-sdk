'use strict';

const baToJSON = require('./ba-to-json');
const rlp = require('rlp');
const _typeof = require('./_typeof');
const toBuffer = require('./to-buffer');
const stripZeroes = require('./strip-zeroes');
const stripHexPrefix = require('./strip-hex-prefix');
const assert = require('../internal/assert');
const {
  Buffer: {
    allocUnsafe,
    isBuffer,
    from: bufferFrom
  }
} = require('safe-buffer');

const defineProperties = (self, fields, data) => {
  self.raw = [];
  self._fields = [];

  // attach the `toJSON`
  self.toJSON = function (label) {
    if (label) {
      return self._fields.reduce((r, v) => {
        r[v] = '0x' + self[v].toString('hex');
        return r;
      }, {});
    }
    return baToJSON(this.raw);
  };

  self.serialize = function serialize() {
    return rlp.encode(self.raw);
  };

  fields.forEach(function (field, i) {
    self._fields.push(field.name);
    function getter() {
      return self.raw[i];
    }
    function setter(v) {
      v = toBuffer(v);

      if (v.toString('hex') === '00' && !field.allowZero) {
        v = allocUnsafe(0);
      }

      if (field.allowLess && field.length) {
        v = stripZeroes(v);
        assert(field.length >= v.length, 'The field ' + field.name + ' must not have more ' + field.length + ' bytes');
      } else if (!(field.allowZero && v.length === 0) && field.length) {
        assert(field.length === v.length, 'The field ' + field.name + ' must have byte length of ' + field.length);
      }

      self.raw[i] = v;
    }

    Object.defineProperty(self, field.name, {
      enumerable: true,
      configurable: true,
      get: getter,
      set: setter
    });

    if (field.default) {
      self[field.name] = field.default;
    }

    // attach alias
    if (field.alias) {
      Object.defineProperty(self, field.alias, {
        enumerable: false,
        configurable: true,
        set: setter,
        get: getter
      });
    }
  });

  // if the constuctor is passed data
  if (data) {
    if (typeof data === 'string') {
      data = bufferFrom(stripHexPrefix(data), 'hex');
    }

    if (isBuffer(data)) {
      data = rlp.decode(data);
    }

    if (Array.isArray(data)) {
      if (data.length > self._fields.length) {
        throw new Error('wrong number of fields in data');
      }

      // make sure all the items are buffers
      data.forEach(function (d, i) {
        self[self._fields[i]] = toBuffer(d);
      });
    } else if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)) === 'object') {
      var keys = Object.keys(data);
      fields.forEach(function (field) {
        if (keys.indexOf(field.name) !== -1) self[field.name] = data[field.name];
        if (keys.indexOf(field.alias) !== -1) self[field.alias] = data[field.alias];
      });
    } else {
      throw new Error('invalid data');
    }
  }
};

module.exports = defineProperties;
