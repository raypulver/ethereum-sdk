'use strict';

const { substr } = String.prototype;

const stripHexPrefix = (s) => substr.call(s, 0, 2) === '0x' ? substr.call(s, 2) : s;

module.exports = stripHexPrefix;
