'use strict';

const padToEven = (v) => String(v).length % 2 ? '0' + String(v) : v;
module.exports = padToEven;
