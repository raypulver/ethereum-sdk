'use strict';

const BN = require('bn.js');

const fromSigned = (num) => new BN(num).fromTwos(256);

module.exports = fromSigned;
