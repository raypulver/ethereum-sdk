'use strict';
const secp256k1 = require('secp256k1');
const setLength = require('./set-length');
const toBuffer = require('./to-buffer');
const {
  Buffer: {
    concat
  }
} = require('safe-buffer');

const ecrecover = (msgHash, v, r, s) => {
  r = toBuffer(r);
  s = toBuffer(s);
  var signature = concat([setLength(r, 32), setLength(s, 32)], 64);
  var recovery = v - 27;
  if (recovery !== 0 && recovery !== 1) {
    throw new Error('Invalid signature v value');
  }
  var senderPubKey = secp256k1.recover(msgHash, signature, recovery);
  return secp256k1.publicKeyConvert(senderPubKey, false).slice(1);
};

module.exports = ecrecover;
module.exports = (hash, v, r, s) => bufferToHex(publicToAddress(ecrecover(toBuffer(hash), toBuffer(v), toBuffer(r), toBuffer(s))));
