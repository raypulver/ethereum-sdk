'use strict';

const { Buffer } = require('safe-buffer');
const { isArray } = Array;
const { slice } = [];

const baToJSON = (ba) => Buffer.isBuffer(ba) ? '0x' + ba.toString('hex') : isArray(ba) ? slice.call(ba) : undefined;

module.exports = baToJSON;
