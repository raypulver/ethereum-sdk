'use strict';

const abiCache = Object.create(null);
const encodeFunctionCall = require('./encode-function-call');
const validateABI = require('./validate-abi');
const saveABI = (name, abi) => {
  abiCache[name] = saveABI.validateABI(abi);
  return (...args) => encodeFunctionCall(abiCache[name], args);
};
const getABI = (name) => abiCache[name];

Object.assign(saveABI, {
  validateABI,
  getABI
});

module.exports = saveABI;
