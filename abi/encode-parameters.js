'use strict';

const abi = require('web3-eth-abi');
const bindKey = require('../internal/bind-key');

module.exports = bindKey(abi, 'encodeParameters');
