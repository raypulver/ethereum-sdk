'use strict';

const { getABI } = require('./save-abi');
const encodeFunctionCall = require('./encode-function-call');

const sigRe = /([^(]+)\((\S*)\)$/;
const { map } = [];
const { split } = String.prototype;

const callFunction = (name, args = []) => {
  let match;
  if ((match = String(name).match(sigRe))) {
    const types = split.call(match[2], ',');
    if (args.length !== types.length) throw Error('Expected' + abi.inputs.length + ' arguments, got ' + args.length);
    return encodeFunctionCall({
      name: match[1],
      inputs: map.call(types, (v) => ({
        name: 'name',
        type: v
      }))
    });
  }
  const abi = getABI(name);
  if (abi === undefined) throw Error('no ABI registered for ' + name);
  if (args.length !== abi.inputs.length) throw Error('Expected ' + abi.inputs.length + ' arguments, got ' + args.length);
  return encodeFunctionCall(abi, args);
};

Object.assign(callFunction, {
  sigRe
});

module.exports = callFunction;
