'use strict';

const bindKey = require('../internal/bind-key');
const abi = require('web3-eth-abi');

module.exports = bindKey(abi, 'isSimplifiedStructFormat');
