'use strict';

const wrap = require('./wrap');
const call = require('./call');

module.exports = wrap.wrapPrecision(call);
