'use strict';

Object.assign(module.exports, {
  mapOwn: require('./map-own'),
  restoreConstructor: require('./restore-constructor'),
  uncurryThis: require('./uncurry-this'),
  curryThis: require('./curry-this'),
  mapOwnNonEnumerable: require('./map-own-non-enumerable'),
  enumeratePrototypeChain: require('./enumerate-prototype-chain'),
  bindObject: require('./bind-object'),
  mapToShifted: require('./map-to-shifted'),
  swapTwoArgs: require('./swap-two-args'),
  applyFunction: require('./apply-function'),
  timeout: require('./timeout'),
  bindObject: require('./bind-object'),
  map: require('./map'),
  getter: require('./getter'),
  property: require('./property'),
  partial: require('./partial'),
  partialRight: require('./partial-right'),
  defaults: require('./defaults'),
  property: require('./property'),
  leftPad: require('./left-pad'),
  negate: require('./negate'),
  bindKey: require('./bind-key'),
  noop: require('./noop'),
  identity: require('./identity')
});
