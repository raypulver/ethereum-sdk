'use strict';

module.exports = (s) => typeof s === 'string';
