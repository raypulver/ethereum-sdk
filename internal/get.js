'use strict';

const castArray = require('./cast-array');
const get = (prop) => (o) => castArray(prop).reduce((r, v) => (r || {})[v], o);

module.exports = get;
