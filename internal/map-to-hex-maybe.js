'use strict';

const mapOwn = require('./map-own');
const bufferToHex = require('../utils/buffer-to-hex');

const mapToHexMaybe = mapOwn((v) => typeof v === 'object' ? bufferToHex(v) : v);

module.exports = mapToHexMaybe;
