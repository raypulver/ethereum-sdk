'use strict';

module.exports = (n) => typeof n === 'number';
