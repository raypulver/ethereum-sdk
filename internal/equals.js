'use strict';

const equals = (value) => (compareTo) => value === compareTo;

module.exports = equals;
