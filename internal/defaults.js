'use strict';

const defaults = (...os) => Object.assign(...os.slice().reverse());

module.exports = defaults;
