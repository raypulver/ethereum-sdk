'use strict';

const { reduce } = [];

const zipObject = (keys) => (values) => reduce.call(keys, (r, key, i) => {
  r[key] = values[i];
  return r;
}, {});
module.exports = zipObject;
