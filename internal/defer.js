'use strict';

const createDeferredFactory = (PromiseClass) => () => {
  let resolve, reject, promise = new PromiseClass((resolve_, reject_) => {
    resolve = resolve_;
    reject = reject_;
  });
  return { resolve, reject, promise };
};

module.exports = createDeferredFactory(Promise);
