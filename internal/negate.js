'use strict';

const negate = (fn) => function (...args) { return !fn.apply(this, args); };
module.exports = negate;
