'use strict';

const set = (o, prop, v) => {
  if (o != null) o[prop] = v;
  return o;
};

module.exports = set;
