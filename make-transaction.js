'use strict';

const Transaction = require('./base-transaction');
const Rational = require('tough-rational');
const mapOwn = require('./internal/map-own');
const bufferToHex = require('./utils/buffer-to-hex');
const addHexPrefix = require('./utils/add-hex-prefix');
const sendTxFromObject = require('./send-tx-from-object');
const getBlock = require('./get-block');
const estimateGas = require('./estimate-gas');
const sendTransaction = require('./send-transaction');
const isUsingBuiltIn = require('./is-using-built-in');
const getTransactionHash = require('./get-transaction-hash');
const calculateContractAddress = require('./calculate-contract-address');

const maybeCoerceToHex = (v) => typeof v === 'object' ? bufferToHex(v) : v;
const mapCoerceToHex = mapOwn(maybeCoerceToHex);

const WrappedTransactionProto = Object.assign(Object.create(Transaction.prototype), {
  getRPC() {
    return this._rpc;
  },
  setRPC(rpc) {
    this._rpc = rpc;
    return this;
  },
  setFrom(from) {
    this._from = from;
    return this;
  },
  getFrom() {
    return this._from;
  },
  sendViaRPC() {
    return sendTxFromObject(this.getRPC(), this);
  },
  computeFromAddress() {
    return bufferToHex(this.from);
  },
  makeSendTransactionCompatible() {
    return mapCoerceToHex({
      gas: this.gasLimit,
      gasPrice: this.gasPrice,
      from: this.getFrom(),
      to: this.to,
      value: this.value,
      data: this.data
    });
  },
  estimateGas() {
    return estimateGas(this.getRPC(), this.makeSendTransactionCompatible());
  },
  async canBeExecuted() {
    const [
      gasUsage,
      { gasLimit }
    ] = await Promise.all([
      this.estimateGas(),
      getBlock(this.getRPC(), 'pending')
    ]);
    if (Rational(gasUsage).geq(Rational(gasLimit).minus(1e4))) return false;
    return true;
  },
  sendViaBuiltIn() {
    return new Promise((resolve, reject) => {
      window.web3.eth.sendTransaction(this.makeSendTransactionCompatible(), (err, result) => err ? reject(err) : resolve(result));
    });
  },
  sendTransaction() {
    return sendTransaction(this.getRPC(), this.makeSendTransactionCompatible());
  },
  sign(...args) {
    Transaction.prototype.sign.apply(this, args);
    return this;
  },
  send() {
    return isUsingBuiltIn(this.getRPC()) ? this.sendViaBuiltIn() : this.sendViaRPC();
  },
  getTransactionHash() {
    return getTransactionHash(this);
  },
  computeContractAddress() {
    return addHexPrefix(calculateContractAddress(this.computeFromAddress(), Number(bufferToHex(this.nonce))));
  }
});

const makeTransaction = (rpc, tx) => Object.setPrototypeOf(new Transaction(tx), WrappedTransactionProto).setRPC(rpc).setFrom(tx.from);

module.exports = makeTransaction;


