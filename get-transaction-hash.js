'use strict';

const { soliditySha3 } = require('./web3-utils');
const bufferToHex = require('./utils/buffer-to-hex');

module.exports = (tx) => soliditySha3({
  t: 'bytes',
  v: bufferToHex(tx.serialize())
});
