'use strict';

const rpcMethod = require('./rpc-method');

module.exports = rpcMethod('eth_call');
