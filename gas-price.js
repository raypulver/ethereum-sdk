'use strict';

const rpcMethod = require('./rpc-method');

const gasPrice = rpcMethod('eth_gasPrice');

module.exports = gasPrice;
