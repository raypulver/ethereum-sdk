'use strict';

const callToPrecision = require('./call-to-precision');
const encodeFunctionCall = require('./abi/encode-function-call');

const getTokenBalance = (rpc, token, holder) => callToPrecision(rpc, {
  to: token,
  data: encodeFunctionCall({
    name: 'balanceOf',
    inputs: [{
      name: 'holder',
      type: 'address'
    }]
  }, [ holder ])
});

module.exports = getTokenBalance;
