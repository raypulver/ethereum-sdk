'use strict';

const rpcMethod = require('./rpc-method');
const getBlockTransactionCountByNumber = rpcMethod('eth_getBlockTransactionCountByNumber');

module.exports = getBlockTransactionCountByNumber;
