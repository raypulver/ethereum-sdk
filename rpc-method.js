'use strict';

const rpcCall = require('./rpc-call');

module.exports = (method) => (rpc, ...args) => rpcCall(rpc, method, args);
