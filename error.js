'use strict';

module.exports = (message, code) => Object.assign(new Error(message), {
  name: 'RPCError',
  code
});
