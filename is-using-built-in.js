'use strict';

const property = require('./internal/property');
const flow = require('./internal/flow');

const isUsingBuiltIn = 'isUsingBuiltIn';

const isUsingBuiltInMethod = flow(property(isUsingBuiltIn), Boolean);

Object.assign(isUsingBuiltInMethod, { isUsingBuiltIn })

module.exports = isUsingBuiltInMethod;
