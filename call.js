'use strict';

const partialRight = require('./internal/partial-right');
const callForBlock = require('./call-for-block');

module.exports = partialRight(callForBlock, 'latest');
