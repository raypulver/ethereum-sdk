'use strict';

const callForBlock = require('./call-for-block');
const wrap = require('./wrap');

module.exports = wrap.wrapAddress(callForBlock);
