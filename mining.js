'use strict';

const rpcMethod = require('./rpc-method')
const mining = rpcMethod('eth_mining');

module.exports = mining;
