'use strict';

const partialRight = require('./internal/partial-right');
const getEtherBalanceForBlockHex = require('./get-ether-balance-for-block-hex');

module.exports = partialRight(getEtherBalanceForBlockHex, 'latest');
